var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var users = [];
var msgs = [];
var nameCount = 0;
var currentUsers = [];


// app.use(express.static(__dirname));
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');


});

// when a user connects
io.on('connection', (socket) => {
  var user = {
    id: socket.id,
    name: null,
    color: 'white'
  };
  
  // sends the history to the socket
  socket.emit('chatHistory', msgs);


  // when a user disconnects it updates the lists
  // and outputs a notice to all sockets
  socket.on('disconnect', () => {
    for (var i = 0; i < users.length; i++) {
      if (users[i] == user) {
        users.splice(i, 1);
      }
      if (currentUsers[i]== user.name) {
        currentUsers.splice(i, 1);
      }
    }
    io.emit('disconnect', user.name );


  });

  // this receives a cookie from the client and determines
  // if the user is new or reconnecting and assigns a name accordingly
  socket.on('send cookie', (cookie) => {
    var newUser = true;
    var taken = false;
    if (cookie != '') {
      newUser = false;
      cookie = cookie.split('=');
      var currentName = cookie[1] 
      for (ind = 0; ind < currentUsers.length; ind++) {
        if (currentUsers[ind] == currentName) {
          taken = true;
        }
      }
    }
    if (!taken && !newUser) {
      user.name = currentName;
      
    } else {
      nameCount++;
      user.name = 'User' + nameCount;
    }
    users.push(user);
    currentUsers.push(user.name);
    socket.emit('sendName', user.name, null, '' );
    socket.emit('userList', currentUsers);
    io.emit('newUser', user.name);
    
    
  });


  // checks if the desired name is available and if it is changes that users name
  // to the desired name
  socket.on('change name', (desiredName, currentName) => {
    var userInd = -1;
    var status = -1;
    var log ='';
    for (ind = 0; ind < currentUsers.length; ind++) {
      check = currentUsers[ind];
      // console.log(check);
      if (check == desiredName || desiredName == currentName) {
        status = 0;
        log = 'Sorry that name is either taken or invalid.';
      }
      if (check == currentName) {
        userInd = ind;
        
      }
    }
    if (userInd != -1) {
      currentUsers[userInd] = desiredName;
    }

    if (status == 0) {
      socket.emit('sendName', desiredName, status, log);
    } else {
      for (ind = 0; ind < users.length; ind++) {
        if (users[ind].name == currentName) {
          users[ind].name = desiredName;
          break;
        }
      }
      status = 1;
      log = 'Your name was succesfully changed!';
      socket.emit('sendName', desiredName, status, log);
      io.emit('userListUpdate', currentName, desiredName);
      var msg = currentName + ' has changed their name to: ' + desiredName + '!';
      newMsg(null, msg, 'white');
      io.emit('notice', msg);
    }
  });

  // changes the color in all chat history and in the user object associated
  // with the user, then tells all sockets so they can update their messages too
  socket.on('change color', (name, red, green, blue) => {
    var str = 'rgb(' + red + ',' + green + ',' + blue + ')';
    for (ind = 0; ind < msgs.length; ind++ ) {
      if (msgs[ind].username == name) {
        msgs[ind].msgColor = str;

      }
    } 

    for (var i = 0; i < users.length; i++) {
      if (users[i].name == name) {
        users[i].color = str;
      }
    }

    io.emit('user color change', name, str);

  });


  // when a chat message is received it finds the color for that user,
  // replaces some text with emotes and then creates a msg object by calling the 
  // newMsg function, then it sends the message to all sockets
  socket.on('chat message', (name, msg) => {


    for (var i = 0; i < users.length; i++) {
      if (users[i].name == name) {
        var color = users[i].color;
      }
    }

    // i got the utf codes from: https://www.w3schools.com/charsets/ref_emoji.asp
    msg = msg.replace(':)', '&#128513').replace(':o', '&#128562').replace(':(', '&#128577')

    msgObj = newMsg(name, msg, color);
    io.emit('chat message', msgObj);
  });

});



http.listen(4000, () => {
  console.log('listening on *:4000');
});


// the function for making the msgObjs
function newMsg(name, msg, color) {
  var date = new Date();
  var hours = date.getHours();
  var minutes = date.getMinutes();
  if (minutes < 10) {
    minutes = '0' + minutes;
  }
  var timeStr = hours+ ':' + minutes;

  for (var i = 0; i < users.length; i++) {
    if (users[i].name == name) {
      var color = users[i].color;
    }
  }
  msg = ': ' + msg;
  var msgObj = {
    time: timeStr,
    username: name,
    msg: msg,
    msgColor : color
  };

  if (msgs.length == 200) {
    msgs.shift();
  } 
  msgs.push(msgObj);
  return msgObj;
  
}